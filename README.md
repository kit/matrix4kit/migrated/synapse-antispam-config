# Synapse AntiSpam Config

This repository contains the global antispam list for the KIT Matrix Homeserver for transparency.

You can check the list in [config.yml](config.yml), which contains all active blocks for

* Whole homeservers (these servers cannot send messages and other events to the kit.edu homeserver, i.e. no rooms or users registered on it)
* Full message contents (if a message matches as a whole, it will be rejected)
* Message patterns (if a message matches the [regex pattern](https://en.wikipedia.org/wiki/Regular_expression), it will be rejected)

Blocks for messages are also enforced within the kit.edu homeserver.